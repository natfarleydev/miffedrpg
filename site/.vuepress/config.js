module.exports = {
  title: 'Miffed RPG',
  description: 'Miffed: an RPG about a serious red panda detective.',
  base: '/miffedrpg/',
  dest: 'public',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'FATE', link: '/game-mechanics.md' }
    ]
  }
};
