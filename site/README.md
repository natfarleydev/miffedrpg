# Miffed

The rain beat against the window. The panda mafia was closing in. I'd just fired
my receptionist. I sat in the dark, unable to pay for the electricity. Bills
were past due. I'd done everything I possibly could. But it didn't matter.

Sometimes, this life felt like a [game](./game-mechanics.md). A
[game](./game-mechanics.md) rigged against me. Wherever the dice fell, that's
what would happen. Nothin' I could do about it.